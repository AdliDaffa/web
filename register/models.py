from datetime import datetime
from django.db import models

# Create your models here.
class JadwalKegiatan(models.Model):
    jadwal = models.DateTimeField(default=datetime.now)
    kegiatan = models.CharField(max_length=50)
    tempat = models.CharField(max_length=50)
    kategori = models.CharField(max_length=50)