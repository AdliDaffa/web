from django import forms

class JadwalForm(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    attrs = {
        'class': 'form-control'
    }

    kegiatan = forms.CharField(label='Kegiatan', required=True, max_length=50, empty_value='Nama Kegiatan', widget=forms.TextInput(attrs=attrs))
    tempat = forms.CharField(label='Tempat', required=True, max_length=50, empty_value='Tempat', widget=forms.TextInput(attrs=attrs))
    jadwal = forms.DateTimeField(label='Jadwal', widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}), required=True, input_formats=['%Y-%m-%dT%H:%M'])
    kategori = forms.CharField(label='Kategori', required=True, max_length=50, empty_value='Kategori', widget=forms.TextInput(attrs=attrs))