from django.urls import path

from .views import index, tambah_kegiatan, jadwal_kegiatan, hapus_kegiatan

app_name = 'register'

urlpatterns = [
    path('', index, name='register'),
    path('tambah_kegiatan/', tambah_kegiatan, name='tambah_kegiatan'),
    path('jadwal_kegiatan/', jadwal_kegiatan, name='jadwal_kegiatan'),
    path('hapus_kegiatan/', hapus_kegiatan, name='hapus_kegiatan'),
]