from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import JadwalForm
from .models import JadwalKegiatan

# Create your views here.
def index(request):
    response = {'jadwal_form': JadwalForm}
    return render(request, 'register.html', response)

def tambah_kegiatan(request):
    form = JadwalForm(request.POST or None)
    response = {}
    if(request.method == 'POST' and form.is_valid()):
        response['kegiatan'] = request.POST['kegiatan']
        response['tempat'] = request.POST['tempat']
        response['jadwal'] = request.POST['jadwal']
        response['kategori'] = request.POST['kategori']
        jadwalkegiatan = JadwalKegiatan(kegiatan=response['kegiatan'], tempat=response['tempat'], jadwal=response['jadwal'], kategori=response['kategori'])
        jadwalkegiatan.save()
        return HttpResponseRedirect('/register/jadwal_kegiatan/')
    else:
        return HttpResponseRedirect('/register/')

def jadwal_kegiatan(request):
    response = {}
    daftar_kegiatan = JadwalKegiatan.objects.all()
    response['daftar_kegiatan'] = daftar_kegiatan
    return render(request, 'jadwal_kegiatan.html', response)

def hapus_kegiatan(request):
    JadwalKegiatan.objects.all().delete()
    return HttpResponseRedirect('/register/jadwal_kegiatan/')